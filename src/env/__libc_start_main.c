#include <elf.h>
#include <poll.h>
#include <fcntl.h>
#include <signal.h>
#include <unistd.h>
#include "syscall.h"
#include "atomic.h"
#include "libc.h"

struct cardinal_program_setup_params
{
	uint16_t ver;
	uint16_t page_size;
	uint32_t argc;
	uint64_t pid;
	uint64_t rng_seed;
	uintptr_t entry_point;
	char **envp;
	char **argv;
};

static void dummy(void) {}
weak_alias(dummy, _init);

extern weak hidden void (*const __init_array_start)(void), (*const __init_array_end)(void);

static void dummy1(void *p) {}
weak_alias(dummy1, __init_ssp);

#define AUX_CNT 38
static size_t auxv_internal[AUX_CNT];

#ifdef __GNUC__
__attribute__((__noinline__))
#endif

static int argc_int = 0;
static char **argv_int = NULL;
static char **envp;

void __init_libc(void *args)
{
	size_t i, *auxv, aux[AUX_CNT] = {0};
	const char *pn = NULL;

	struct cardinal_program_setup_params *init_params = (struct cardinal_program_setup_params *)args;
	argc_int = (int)init_params->argc;
	argv_int = (char **)init_params->argv;
	envp = init_params->envp;

	if (argv_int != NULL && argc_int > 0)
		pn = (const char *)argv_int[0];

	__environ = init_params->envp;
	for (i = 0; envp[i]; i++)
		;
	libc.auxv = auxv = (void *)auxv_internal;

	for (i = 0; i < AUX_CNT; i++)
		auxv_internal[i] = 0;

	auxv_internal[AT_RANDOM] = (size_t)init_params->rng_seed;
	auxv_internal[AT_ENTRY] = (size_t)init_params->entry_point;
	auxv_internal[AT_UID] = auxv_internal[AT_EUID] = (size_t)0;
	auxv_internal[AT_GID] = auxv_internal[AT_EGID] = (size_t)init_params->pid;
	auxv_internal[AT_PAGESZ] = (size_t)init_params->page_size;

	__hwcap = aux[AT_HWCAP];
	if (aux[AT_SYSINFO])
		__sysinfo = aux[AT_SYSINFO];
	libc.page_size = aux[AT_PAGESZ];

	if (!pn)
		pn = (void *)aux[AT_EXECFN];
	if (!pn)
		pn = "";
	__progname = __progname_full = pn;
	for (i = 0; pn[i]; i++)
		if (pn[i] == '/')
			__progname = pn + i + 1;

	__init_tls(aux);
	__init_ssp((void *)aux[AT_RANDOM]);

	if (aux[AT_UID] == aux[AT_EUID] && aux[AT_GID] == aux[AT_EGID] && !aux[AT_SECURE])
		return;

	struct pollfd pfd[3] = {{.fd = 0}, {.fd = 1}, {.fd = 2}};
	int r =
#ifdef SYS_poll
		__syscall(SYS_poll, pfd, 3, 0);
#else
		__syscall(SYS_ppoll, pfd, 3, &(struct timespec){0}, 0, _NSIG / 8);
#endif
	if (r < 0)
		a_crash();
	for (i = 0; i < 3; i++)
		if (pfd[i].revents & POLLNVAL)
			if (__sys_open("/dev/null", O_RDWR) < 0)
				a_crash();
	libc.secure = 1;
}

static void libc_start_init(void)
{
	_init();
	uintptr_t a = (uintptr_t)&__init_array_start;
	for (; a < (uintptr_t)&__init_array_end; a += sizeof(void (*)()))
		(*(void (**)(void))a)();
}

weak_alias(libc_start_init, __libc_start_init);

typedef int lsm2_fn(int (*)(int, char **, char **), void *);
static lsm2_fn libc_start_main_stage2;

int __libc_start_main(int (*main)(int, char **, char **), void *args)
{
	/* External linkage, and explicit noinline attribute if available,
	 * are used to prevent the stack frame used during init from
	 * persisting for the entire process lifetime. */
	__init_libc(args);

	/* Barrier against hoisting application code or anything using ssp
	 * or thread pointer prior to its initialization above. */
	lsm2_fn *stage2 = libc_start_main_stage2;
	__asm__(""
			: "+r"(stage2)
			:
			: "memory");
	return stage2(main, args);
}

static int libc_start_main_stage2(int (*main)(int, char **, char **), void *args)
{
	__libc_start_init();

	/* Pass control to the application */
	exit(main(argc_int, argv_int, envp));
	return 0;
}
